<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension membergroup-newsletter-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */

namespace Srhinow\MemberGroupNewsletterBundle\EventListener\Hook;

use Contao\CoreBundle\ServiceAnnotation\Hook;
use Contao\Database;
use Contao\MemberGroupModel;
use Contao\Module;
use Contao\StringUtil;

/**
 * @Hook("createNewUser")
 */
class CreateNewUserListener
{
    /**
     * weist dem neuen Member alle Newsletter zu die in den gewählten Gruppen eingestellt wurden
     * @param int $userId
     * @param array $userData
     * @param Module $module
     */
    public function onCreateNewUser(int $userId, array $userData, Module $module): void
    {
        $arrGroups = StringUtil::deserialize($userData['groups'], true);

        // Return if there are no newsletters
        if (!\is_array($arrGroups)) {
            return;
        }

        $arrNewsletters = $this->getNewsletterFromUserGroups($arrGroups);
        if (!\is_array($arrNewsletters) || \count($arrNewsletters) < 1) {
            return;
        }

        $time = time();
        $Database = Database::getInstance();

        // Add recipients
        foreach ($arrNewsletters as $intNewsletter) {
            $intNewsletter = (int) $intNewsletter;

            if ($intNewsletter < 1) {
                continue;
            }

            $objRecipient = $Database
                ->prepare('SELECT COUNT(*) AS count FROM tl_newsletter_recipients WHERE pid=? AND email=?')
                ->execute($intNewsletter, $userData['email'])
            ;

            if ($objRecipient->count < 1) {
                $Database
                    ->prepare("INSERT INTO tl_newsletter_recipients SET pid=?, tstamp=$time, email=?, addedOn=$time")
                    ->execute($intNewsletter, $userData['email'])
                ;
            }
        }
    }

    /**
     * holt unique alle die in den Groups zugewiesenen Newsletter-Channel und gibt diese gesammelt zurück
     * @param $arrGroups
     * @return array
     */
    protected function getNewsletterFromUserGroups($arrGroups)
    {
        $arrReturn = [];
        if(!is_array($arrGroups) || count($arrGroups) < 1) {
            return $arrReturn;
        }

        foreach ($arrGroups as $intGroup) {
            $objMemberGroup = MemberGroupModel::findByPk($intGroup);
            if (null === $objMemberGroup) {
                continue;
            }

            $arrGroupNewsletter = unserialize($objMemberGroup->newsletters);
            if(!is_array($arrGroupNewsletter) || count($arrGroupNewsletter) < 1) {
                continue;
            }

            $arrReturn = array_unique(array_merge($arrReturn, $arrGroupNewsletter));
        }

        return $arrReturn;
    }
}
