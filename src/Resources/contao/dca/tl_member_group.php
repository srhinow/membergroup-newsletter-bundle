<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension membergroup-newsletter-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */

$GLOBALS['TL_DCA']['tl_member_group']['palettes']['default'] .= ';{newsletter_legend},newsletters';

/*
 * Fields
 */
// Add fields to tl_module
$GLOBALS['TL_DCA']['tl_member_group']['fields']['newsletters'] = [
    'exclude' => true,
    'inputType' => 'checkbox',
    'foreignKey' => 'tl_newsletter_channel.title',
    'eval' => ['multiple' => true],
    'sql' => 'blob NULL',
];
