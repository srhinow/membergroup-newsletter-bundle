<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension membergroup-newsletter-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */
