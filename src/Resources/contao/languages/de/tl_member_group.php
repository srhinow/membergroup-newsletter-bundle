<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension membergroup-newsletter-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */

/*
 * Legends
 */
$GLOBALS['TL_LANG']['tl_member_group']['newsletter_legend'] = 'Newsletter-Einstellungen';

/*
 * Fields
 */
$GLOBALS['TL_LANG']['tl_member_group']['newsletters'] = [
    'Newsletter',
    'Diese Newsletter werden bei der Registrierung automatisch dem Account hinzugefügt deren Group der Account gehört.',
];
